"""
The Movie Maker
Fiji script which creates movie files based on time series image stacks for publication and presentation purposes.

Note : This script is tested on single channel image files only.
For multi-channel time series, channels may be merged or split prior to running the script.

Operation
Open time series image stack and select the channels that will be incorporated in the final movie.
Merge desired channels and run Movie Maker script.

The script will :
- Register the time series using the StackReg plugin(Biomedical Imaging Group,
  EPFL, (bigwww.epfl.ch/thevenaz/stackreg/))
- Crop the image (user-assisted)
- Add a timer onto each image of the time series (if desired)
- Add a scale bar (if desired)
- Add a text label on the time series (if desired)
- Save the new image as a movie (avi format)
 and/or as an image stack (tiff format)
"""

from ij import IJ
from ij.gui import NonBlockingGenericDialog, Toolbar

__author__ = "Daniel Garneau"
__credits__ = ["Daniel Garneau", "Alexandre Marechal"]
__license__ = "GPL v3"
__version__ = "1.0.1"
__maintainer__ = "Daniel Garneau"
__email__ = "daniel.garneau@usherbrooke.ca"

NO = 0
YES = 1
CANCEL = 2


class TheMovieMaker:

    def __init__(self):
        self.imp = None
        # how to save image (Tiff, Avi)
        self.save_image = (False, False)

    @staticmethod
    def exit_dialog():
        """
        Ask if the user really wants to exit the plugin. Stop the script if the user choose 'Yes'.
        If 'No' or 'Cancel' is selected, the user return to the script
        :return: True if the user click 'Yes' and False for 'No'
        :rtype: bool
        """

        dial = NonBlockingGenericDialog('Exit the plugin ?')
        dial.enableYesNoCancel()
        dial.hideCancelButton()
        dial.addMessage('Are you sure you want to exit the plugin ?')
        dial.showDialog()
        if dial.wasOKed():
            return True
        else:
            return False

    def registration_dialog(self):
        """
        Ask the user if a registration of the image stack must be made.
        :return: YES, NO or CANCEL
        :rtype: int
        """
        plugin_exit = False
        while not plugin_exit:
            dial = NonBlockingGenericDialog('Registration')
            dial.addMessage('Do you want to register the image stack (StackReg plugin required).')
            dial.enableYesNoCancel()
            dial.showDialog()

            if dial.wasOKed():
                return YES
            if dial.wasCanceled():
                if self.exit_dialog():
                    return CANCEL
                else:
                    plugin_exit = False
            else:
                return NO

    def stackreg_dialog(self):
        """
        This dialog will pop if the user want to register the image but does not have StackReg install
        :return: True if 'OK' and False if 'Cancel'
        :rtype: bool
        """
        plugin_exit = False
        while not plugin_exit:
            dial = NonBlockingGenericDialog('StackReg not found')
            dial.addMessage('StackReg is not installed\n'
                            ' \n'
                            'To install StackReg in Fiji:\n'
                            '- Go to "Help > Update..."\n'
                            '- Click on "Manage update sites"\n'
                            '- On the new window, check the box beside "BIG-EPFL" and click "Close"\n'
                            '- Click "Apply changes"\n'
                            '- Restart Fiji'
                            ' \n'
                            'Click "OK" if you want to continue without registering the image stack')

            dial.showDialog()

            if dial.wasOKed():
                return True
            else:
                if self.exit_dialog():
                    return False
                else:
                    plugin_exit = False

    def crop_dialog(self):
        """
        Select the rectangle selection tool and create a dialog box that ask the user to select where to crop the image.
        If no selection is made and the OK button is pressed, no crop is applied.
        :return: YES, NO or CANCEL
        :rtype: int
        """

        plugin_exit = False
        while not plugin_exit:
            IJ.setTool(Toolbar.RECTANGLE)
            dial = NonBlockingGenericDialog('Crop image')
            dial.enableYesNoCancel()
            dial.addMessage("Do you want to crop the image ? If yes, select the region of interest and click 'Yes'.")
            dial.showDialog()

            if dial.wasOKed():
                return YES
            elif dial.wasCanceled():
                if self.exit_dialog():
                    return CANCEL
                else:
                    plugin_exit = False
            else:
                return NO

    def timer_dialog(self):
        """
        Ask the user if timer label must be added to the movie
        :return: YES, NO or CANCEL
        :rtype: int
        """
        plugin_exit = False

        while not plugin_exit:
            dial = NonBlockingGenericDialog('Label movie')
            dial.enableYesNoCancel()
            dial.addMessage('Do you want to add a timer on the movie ?')
            dial.addMessage('If you want to customize the timer, these patterns can be used:\n'
                            '"HH" = hours\n'
                            '"mm" = minutes\n'
                            '"ss" = seconds\n'
                            '"SS" = milliseconds')
            dial.addMessage('Exemple: (HH:mm:ss) or ss.SSS')
            dial.showDialog()

            if dial.wasOKed():
                return YES
            elif dial.wasCanceled():
                if self.exit_dialog():
                    return CANCEL
                else:
                    plugin_exit = False
            else:
                return NO

    def scale_bar_dialog(self):
        """
        Ask the user to add a scale bar.
        :return: YES, NO or CANCEL
        :rtype: int
        """

        plugin_exit = False
        while not plugin_exit:
            dial = NonBlockingGenericDialog('Scale bar')
            dial.enableYesNoCancel()
            dial.addMessage('Do you want to add a scale bar on the movie ?')
            dial.showDialog()

            if dial.wasOKed():
                return YES
            if dial.wasCanceled():
                if self.exit_dialog():
                    return CANCEL
                else:
                    plugin_exit = False
            else:
                return NO

    def label_stack_dialog(self):
        """
        Ask the user if another label must be added to the movie
        :return: YES, NO or CANCEL
        :rtype: int
        """

        plugin_exit = False

        while not plugin_exit:
            dial = NonBlockingGenericDialog('Label movie')
            dial.enableYesNoCancel()
            dial.addMessage('Do you want to add text on the movie ?')
            dial.showDialog()

            if dial.wasOKed():
                return YES
            if dial.wasCanceled():
                if self.exit_dialog():
                    return CANCEL
                else:
                    plugin_exit = False
            else:
                return NO

    def save_dialog(self):
        """
        Ask the user how the movie must be save (tiff or avi)
        :return: Tuple of bool (tiff, avi)
        """

        plugin_exit = False

        while not plugin_exit:
            dial = NonBlockingGenericDialog('Save movie')
            dial.addMessage("How does the movie must be save ?")
            dial.addCheckboxGroup(2, 1, ('TIFF image stack', 'AVI movie'), (False, False))
            dial.showDialog()

            if dial.wasOKed():
                choice_tiff = dial.getNextBoolean()
                choice_avi = dial.getNextBoolean()
                self.save_image = (choice_tiff, choice_avi)
                return YES

            else:
                if self.exit_dialog():
                    return CANCEL

    def run(self):
        """
        Start the analysis
        """
        self.imp = IJ.getImage()
        lut = self.imp.getLuts()

        # Image registration
        reg_state = self.registration_dialog()
        if reg_state == YES:
            try:
                import StackReg_
            except ImportError:
                if not self.stackreg_dialog():
                    return
            else:
                IJ.showMessage('On the following window, select the transformation type for the registration')
                IJ.showStatus('Processing image registration')
                StackReg_().run("transformation=[Rigid Body]")
                # add lut back after StackReg_
                if len(lut) > 0:
                    self.imp.setLut(lut[0])
        elif reg_state == CANCEL:
            return

        # Crop image
        crop = self.crop_dialog()
        if crop == YES:
            self.imp.hide()
            label = self.imp.getTitle()
            self.imp = self.imp.duplicate()
            self.imp.setTitle(label)
            self.imp.show()
        elif crop == CANCEL:
            return

        IJ.run(self.imp, "RGB Color", "")

        # Add a timer on each frame.
        timer = self.timer_dialog()
        if timer == YES:
            IJ.run(self.imp, "Series Labeler", "")
        elif timer == CANCEL:
            return

        # Add scale bar
        scale = self.scale_bar_dialog()
        if scale == YES:
            IJ.run(self.imp, "Scale Bar...", "")
        elif scale == CANCEL:
            return

        # Add text label to the movie
        label = self.label_stack_dialog()
        if label == YES:
            IJ.run(self.imp, "Label...", "")
        elif label == CANCEL:
            return

        # Save file as AVI and or Tiff
        save_choice = self.save_dialog()

        if save_choice == YES:
            if self.save_image[0]:
                IJ.saveAs(self.imp, 'Tiff', '')

            if self.save_image[1]:
                IJ.saveAs(self.imp, 'Avi', '')

        elif save_choice == CANCEL:
            return

TheMovieMaker().run()
