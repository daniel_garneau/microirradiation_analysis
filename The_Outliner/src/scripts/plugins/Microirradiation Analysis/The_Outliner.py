"""
The Outliner

This Fiji script can works in two modes: single image mode or batch mode

In single image mode, the plugin will process a single image already open in Fiji. In batch mode, a directory is
selected and all images in this folder will be processed.

This script processes multi-channel images and saves each channel individually with a nuclear outline (based on a
nucleic acid stain channel e.g. DAPI, Hoechst...).

A merge of all channels with the nuclear outline is also created. The nuclear outline enhances the visualization of
protein recruitment to DNA lesions by omitting the nucleic acid stain channel while conserving nuclear space definition.

The goal of this script is to automatically create images for publication and presentation purposes.
"""

from ij import IJ, ImagePlus, CompositeImage, WindowManager
from ij.gui import NonBlockingGenericDialog, Toolbar
from ij.io import Opener, DirectoryChooser
from ij.measure import ResultsTable
from ij.plugin import RGBStackMerge, ImageCalculator
from ij.plugin.filter import ParticleAnalyzer as PA
from ij.plugin.frame import ThresholdAdjuster
import os
import glob
import random

__author__ = "Daniel Garneau"
__credits__ = ["Daniel Garneau", "Alexandre Marechal"]
__license__ = "GPL v3"
__version__ = "1.0.3"
__maintainer__ = "Daniel Garneau"
__email__ = "daniel.garneau@usherbrooke.ca"


class BatchProcessing(object):
    """
    Manage the images for batch image analysis
    """
    def __init__(self, image_dir, image_formats):

        self.image_dir = image_dir
        self.image_formats = image_formats
        self.image_list = []
        self.current_image = None

        self.get_all_images()
        self.current_image_number = 0
        self.working_image_list = self.image_list[:]

    def get_all_images(self):
        """
        Get all images in main directory and in subdirectories
        :return: List of all image paths
        :rtype: list
        """

        for root, dirs, files in os.walk(self.image_dir):
            for image_format in self.image_formats:
                dir_images = glob.glob(os.path.join(root, "*.%s" % image_format))
                self.image_list += dir_images

        return self.image_list

    def get_total_images_number(self):
        """
        Get the total number of images.
        :return: number of images
        :rtype: int
        """
        return len(self.image_list)

    def get_current_image_number(self):
        """
        Get the current image number (1 based)
        :return: current image number
        :rtype: int
        """
        return self.current_image_number

    def get_next_image(self):
        """
        Get the next image in list and open it using BioFormats. Sets the current_image.
        :return: Image
        :rtype: ImagePlus
        """

        # close the old image
        if self.current_image is not None:
            self.current_image.close()

        # Open the new image
        image_path = self.working_image_list.pop(0)

        self.current_image = Opener.openUsingBioFormats(image_path)

        self.current_image_number += 1

        return self.current_image

    def get_random_image(self):
        """
        Get a random image in the original list of images.
        :return: Image
        :rtype: ImagePlus
        """

        # close the old image
        if self.current_image is not None:
            self.current_image.close()

        image_path = random.choice(self.image_list)

        imp = Opener.openUsingBioFormats(image_path)

        return imp

    def get_image_list(self):
        """
        Return a list of the paths of all images.
        :return: List of image paths
        :rtype: list
        """
        return self.image_list

    def get_working_image_list(self):
        """
        Return the list of the paths of all the remaining images to analyze.
        :return: List of image paths
        :rtype: list
        """
        return self.working_image_list

    def image_left_in_working_list(self):
        """
        Check if there is image left in the working list.
        :return: True if there is image in working list, otherwise return False
        :rtype: bool
        """
        if self.working_image_list:
            return True
        else:
            return False


class TheOutliner:

    def __init__(self):
        self.image_directory = ""
        self.save_directory = ""
        self.formats_list = ['tif', 'tiff', 'oir', 'czi', 'zvi']
        self.bp = None

        # images characteristics
        self.measure_unit = ""
        self.nuclei_channel = 1
        self.min_area = 0
        self.max_area = 100000
        self.min_circularity = 0
        self.max_circularity = 1
        self.threshold_method = None
        self.watershed = False

        # save parameters
        self.overwrite = False
        self.save_format = 'tiff'

        self.imp = None
        self.imp_calibration = None
        self.luts = None
        self.display_range_min = None
        self.display_range_max = None
        self.blur = 2

        self.single = "Single image"
        self.batch = "Batch"
        self.mode = self.single

    @staticmethod
    def exit_dialog():
        """
        Ask if the user really wants to exit the plugin. Stop the script if the user choose 'Yes'.
        If 'No' or 'Cancel' is selected, the user return to the script
        :return: True if the user click 'Yes' and False for 'No'
        :rtype: bool
        """

        dial = NonBlockingGenericDialog('Exit the plugin ?')
        dial.enableYesNoCancel()
        dial.hideCancelButton()
        dial.addMessage('Are you sure you want to exit the plugin ?')
        dial.showDialog()
        if dial.wasOKed():
            return True
        else:
            return False

    def select_mode_dialog(self):
        """
        Select if the script start in single image or batch mode
        :return: True if 'OK', False if 'Cancel'
        :rtype: bool
        """

        plugin_exit = False

        if WindowManager.getImageCount() == 0:
            self.mode = self.batch

        while not plugin_exit:
            dial = NonBlockingGenericDialog('Select mode')
            dial.addRadioButtonGroup('In which mode do you want to use the plugin ?', (self.single, self.batch), 2, 1,
                                     self.mode)
            dial.showDialog()

            if dial.wasOKed():
                self.mode = dial.getNextRadioButton()
                return True
            else:
                if self.exit_dialog():
                    return False
                else:
                    plugin_exit = False

    def select_image_directory_dialog(self):
        """
        Select the directory where the original images are.
        :return: True if a a path is selected, False if canceled or no path selected.
        :rtype: bool
        """

        if IJ.isMacOSX():
            IJ.showMessage('Select directories',
                           'In the following dialog, please select the folder from which images will'
                           ' be taken for analysis')
        self.image_directory = DirectoryChooser('Select the image directory').getDirectory()
        if self.image_directory is None:
            return False
        return True

    def select_save_directory(self):
        """
        Select the directory where the new images will be saved.
        :return: True if a a path is selected, False if canceled or no path selected.
        :rtype: bool
        """

        if IJ.isMacOSX():
            IJ.showMessage('Select directories', 'In the following dialog, please select the folder in which you want'
                                                 ' to save the outlined images')
        self.save_directory = DirectoryChooser('Select where to save images').getDirectory()
        if self.save_directory is None:
            return False
        return True

    def file_format_dialog(self):
        """
        Ask the user to input the desired file formats.
        :return: False if canceled, True if OK
        :rtype: bool
        """
        plugin_exit = False

        while not plugin_exit:
            default_formats = 'tif, tiff, oir, czi, zvi'
            dial = NonBlockingGenericDialog('Image format')
            dial.addMessage("Insert the image formats that you want to analyze (separated by comma)")
            dial.addStringField('', default_formats, 50)
            dial.addMessage('Example: jpg, png, tif')
            dial.showDialog()

            if dial.wasOKed():
                formats_string = dial.getNextString()
                formats_string = formats_string.replace(' ', '')
                self.formats_list = formats_string.split(',')
                plugin_exit = True

            else:
                if self.exit_dialog():
                    return False
                else:
                    plugin_exit = False
        return True

    def segmentation_dialog(self):
        """
        Create a dialog window asking for basic information for the segmentation and show an image to perform
        measurements
        :return: False if canceled, True if OK
        :rtype: bool
        """
        plugin_exit = False
        dial_location = None
        IJ.setTool(Toolbar.OVAL)

        while not plugin_exit:
            # Show image to help with measures and get image calibration. If an image is already open, use
            # this image.

            if self.mode == self.batch:
                imp_test = CompositeImage(self.bp.get_random_image())
                imp_test.setMode(CompositeImage.COLOR)
                if self.luts is not None:
                    imp_test.setLuts(self.luts)
                self.imp_calibration = imp_test.getCalibration()
                self.measure_unit = self.imp_calibration.getUnit()
                imp_test.setC(self.nuclei_channel)
                imp_test.show()
            else:
                self.imp.deleteRoi()
                self.imp_calibration = self.imp.getCalibration()
                self.measure_unit = self.imp_calibration.getUnit()
                self.imp.setC(self.nuclei_channel)

            # Create the dialog
            dial = NonBlockingGenericDialog('Object properties')
            if self.mode == self.batch:
                dial.enableYesNoCancel('OK', 'New image')
            dial.addMessage("Adjust the lookup table for each channel if necessary"
                            " and enter the information below.\n \n")
            dial.addNumericField('Channel number corresponding to nuclei', self.nuclei_channel, 0)
            dial.addNumericField('Minimum area of objects (square %s)' % self.measure_unit, self.min_area, 0)
            dial.addNumericField('Maximum area of objects (square %s)' % self.measure_unit, self.max_area, 0)
            dial.addNumericField('Minimum circularity', self.min_circularity, 0)
            dial.addNumericField('Maximum circularity', self.max_circularity, 0)
            if dial_location is not None:
                dial.setLocation(int(dial_location.getX()), int(dial_location.getY()))
            dial.showDialog()

            if dial.wasOKed():
                self.nuclei_channel = int(dial.getNextNumber())
                self.min_area = dial.getNextNumber()
                self.max_area = dial.getNextNumber()
                self.min_circularity = dial.getNextNumber()
                self.max_circularity = dial.getNextNumber()

                if self.mode == self.batch:
                    self.luts = imp_test.getLuts()
                    imp_test.close()

                plugin_exit = True

            elif dial.wasCanceled():
                if self.exit_dialog():
                    return False
                else:
                    if self.mode == self.batch:
                        imp_test.close()
                    plugin_exit = False

            else:
                # When a new image must be shown (batch mode only)
                self.nuclei_channel = int(dial.getNextNumber())
                self.min_area = dial.getNextNumber()
                self.max_area = dial.getNextNumber()
                self.min_circularity = dial.getNextNumber()
                self.max_circularity = dial.getNextNumber()
                self.overwrite = dial.getNextBoolean()
                self.luts = imp_test.getLuts()

                dial_location = dial.getLocation()

                imp_test.close()
                plugin_exit = False
        return True

    def threshold_dialog(self):
        """
        Create a dialog window asking for the autothreshold method to use and if watershed must be
        applied or not. Show an image to test the autothreshold methods.
        :return: False if canceled, True if OK
        :rtype: bool
        """

        plugin_exit = False
        dial_location = None

        while not plugin_exit:
            # Show image to help with measures and get image calibration
            if self.mode == self.batch:
                imp_test = CompositeImage(self.bp.get_random_image())
                imp_test.setLuts(self.luts)
                imp_test.setC(self.nuclei_channel)

            else:
                self.imp.deleteRoi()
                imp_test = self.imp.duplicate()

            imp_test.deleteRoi()
            imp_test.getProcessor().blurGaussian(self.blur)
            imp_test.show()
            IJ.run(imp_test,  "Threshold...", "")

            # Create the dialog
            dial = NonBlockingGenericDialog('Threshold method')
            if self.mode == self.batch:
                dial.enableYesNoCancel('OK', 'New image')
            dial.addMessage('Using the "Threshold" dialog, select the best autothreshold method for your cells'
                            ' and click "OK".')
            dial.addCheckbox('Use watershed (Use this to declump cells. Keep in mind it can lead to cell getting '
                             'cut in the segmentation)', self.watershed)
            if dial_location is not None:
                dial.setLocation(int(dial_location.getX()), int(dial_location.getY()))
            dial.showDialog()

            if dial.wasOKed():
                self.threshold_method = ThresholdAdjuster.getMethod()
                self.watershed = dial.getNextBoolean()
                imp_test.close()
                plugin_exit = True

            elif dial.wasCanceled():
                if self.exit_dialog():
                    return False
                else:
                    imp_test.close()
                    plugin_exit = False

            else:
                self.watershed = dial.getNextBoolean()
                dial_location = dial.getLocation()
                imp_test.close()
                plugin_exit = False

        return True

    def save_dialog(self):
        """
        Ask questions about the save file formats and overwrite
        :return: False if canceled, True if OK
        :rtype: bool
        """

        plugin_exit = False

        while not plugin_exit:
            dial = NonBlockingGenericDialog('Save options')
            dial.addChoice("Select the saved image format", ('tiff', 'png', 'jpg'), self.save_format)
            dial.addCheckbox('Overwrite images if they already exist.', self.overwrite)
            dial.showDialog()

            if dial.wasOKed():
                self.save_format = dial.getNextChoice()
                self.overwrite = dial.getNextBoolean()
                return True
            else:
                if self.exit_dialog():
                    return False
                else:
                    plugin_exit = False

    def save_image(self, imp, image_path):
        """
        Save image. If overwrite is set to False and the image already exists, a number
        is added at the end of the of the image filename.
        :param imp: image
        :type imp: ImagePlus
        :param image_path: image path
        :type image_path: basestring
        :return: None
        """
        if (not self.overwrite) and os.path.isfile(image_path):
            n = 1
            directory, filename = os.path.split(image_path)
            image_name = os.path.splitext(filename)[0]
            while os.path.isfile(image_path):
                new_filename = "%s_%03d.%s" % (image_name, n, self.save_format)
                image_path = os.path.join(directory, new_filename)
                n += 1

        IJ.saveAs(imp, self.save_format, image_path)
        return None

    def run(self):
        """
        Start the analysis.
        """

        # Select if the plugin must be used in single image or in batch mode
        mode = self.select_mode_dialog()
        if not mode:
            return

        # If in batch mode, select image directory.
        if self.mode == self.batch:
            image_dir = self.select_image_directory_dialog()
            if not image_dir:
                return

        # Select where to save the new images
        save_dir = self.select_save_directory()
        if not save_dir:
            return

        # If in batch mode, ask for desired file formats and get a list of images
        if self.mode == self.batch:
            file_format = self.file_format_dialog()
            if not file_format:
                return

            # Get a list of all images matching the desired file formats
            self.bp = BatchProcessing(self.image_directory, image_formats=self.formats_list)

        # If in single image mode, get the image
        if self.mode == self.single:
            self.imp = CompositeImage(IJ.getImage())

        # Get measurements for segmentation
        segmentation = self.segmentation_dialog()
        if not segmentation:
            return

        # If in single image mode
        if self.mode == self.single:
            self.imp.setImage(IJ.getImage())

        # convert units to pixels
        square_unit = self.imp_calibration.pixelHeight * self.imp_calibration.pixelWidth
        self.min_area = self.min_area / square_unit
        self.max_area = self.max_area / square_unit

        # Set the thresholding method and ask if watershed must be made.
        threshold = self.threshold_dialog()
        if not threshold:
            return

        save_format = self.save_dialog()
        if not save_format:
            return

        if self.mode == self.batch:
            imp_list = self.bp.image_left_in_working_list()
        else:
            imp_list = True

        IJ.log('Start analysis')
        # Process all images until the image list is empty
        while imp_list:
            # If in batch mode, get a new image
            if self.mode == self.batch:
                self.imp = CompositeImage(self.bp.get_next_image())
                self.imp.setLuts(self.luts)
                self.imp.show()

            image_name = os.path.splitext(self.imp.getTitle())[0]

            if self.mode == self.batch:
                IJ.log("Processing image %s/%s" % (self.bp.get_current_image_number(),
                                                   self.bp.get_total_images_number()))

            # Blur and threshold the nuclei channel
            self.imp.setC(self.nuclei_channel)
            imp_segmentation = ImagePlus('Segmentation', self.imp.getProcessor().duplicate())
            imp_segmentation.getProcessor().blurGaussian(self.blur)
            IJ.setAutoThreshold(imp_segmentation, "%s dark" % self.threshold_method)
            IJ.run(imp_segmentation, "Convert to Mask", "method=%s backgrond=Dark black" % self.threshold_method)
            if self.watershed:
                IJ.run(imp_segmentation, "Watershed", "slice")

            # Segmentation of the nuclei
            rt = ResultsTable()
            seg_options = PA.SHOW_OUTLINES | PA.INCLUDE_HOLES | PA.EXCLUDE_EDGE_PARTICLES
            segmentation = PA(seg_options, ResultsTable.AREA, rt, self.min_area, self.max_area, self.min_circularity,
                              self.max_circularity)
            segmentation.setHideOutputImage(True)
            segmentation.setFontColor("white")
            segmentation.analyze(imp_segmentation)
            imp_segmentation.close()

            # Create the outline image on black background
            imp_roi = segmentation.getOutputImage()
            roi_ip = imp_roi.getProcessor()
            roi_ip.multiply((2**imp_roi.getBitDepth())-1)
            roi_ip.invert()
            imp_roi.setProcessor(roi_ip.convertToRGB())

            # Create the single channel and multichannel outlines except for the nuclei channel
            imp_merge = imp_roi.duplicate()

            channel_number = self.imp.getNChannels()

            for c in range(channel_number):
                channel = c + 1

                # create single channel outlined images and save them
                self.imp.setC(channel)
                imp_new = ImagePlus('New_image', self.imp.getProcessor().duplicate().convertToRGB())
                imp_new.setCalibration(self.imp_calibration)
                imp_new_outline = ImageCalculator().run('Add create', imp_new, imp_roi)
                image_path = os.path.join(self.save_directory, "%s_c%s.%s" % (image_name, channel, self.save_format))
                self.save_image(imp_new_outline, image_path)
                imp_new.close()
                imp_new_outline.close()

                # create multichannel outlined image for images with more than two channels
                if channel != self.nuclei_channel and channel_number > 2:
                    imp_merge = ImageCalculator().run('Add create', imp_merge, imp_new)

            # Save multichannel outline image for images with more than two channels
            if channel_number > 2:
                imp_merge.setCalibration(self.imp_calibration)
                image_path = os.path.join(self.save_directory, "%s_%s.%s" % (image_name, 'merge', self.save_format))
                self.save_image(imp_merge, image_path)
                imp_merge.close()

            imp_roi.close()
            if self.mode == self.batch:
                IJ.showProgress(self.bp.get_current_image_number(), self.bp.get_total_images_number())
                self.imp.close()

            if self.mode == self.batch:
                imp_list = self.bp.image_left_in_working_list()
            else:
                imp_list = False

        IJ.log('Done')
        IJ.log('All images saved in the folder : %s' % self.save_directory)
        IJ.beep()
        IJ.showMessage('The Outliner', 'All images processed')


TheOutliner().run()
